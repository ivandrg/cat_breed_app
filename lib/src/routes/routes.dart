import 'package:cats_app/src/pages/detail_page.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    'detail': (BuildContext context) => const DetailPage(),
  };
}
