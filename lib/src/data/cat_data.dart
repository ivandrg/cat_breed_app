import 'dart:convert';
import 'dart:io';

import 'package:cats_app/src/models/cat_data_model.dart';
import 'package:http/http.dart' as http;

class DataCat {
  getDataCats() async {
    List cats = [];
    final queryParameters = {
      'x-api-key': 'bda53789-d59e-46cd-9bc4-2936630fde39',
    };
    final url = Uri.https(
      'api.thecatapi.com',
      'v1/breeds',
      queryParameters,
    );
    try {
      final response = await http.get(
        url,
        headers: {
          "Accept": "application/json",
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      );
      if (response.statusCode != 200) {
        throw const SocketException('Error en el servidor');
      }
      final data = jsonDecode(response.body);
      for (var element in data) {
        cats.add(CatDataModel.fromMap(element));
      }
      return cats;
    } on SocketException catch (e) {
      throw Failure(e.message);
    }
  }
}

class Failure {
  final String message;

  Failure(this.message);

  @override
  String toString() => message;
}
