import 'package:cats_app/src/cubit/search_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transparent_image/transparent_image.dart';

class SearchListviewWidget extends StatelessWidget {
  const SearchListviewWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchCubit, SearchState>(
      builder: (context, state) {
        if (state is SearchInitial) {
          return const Center(
            child: Text('Busca una raza'),
          );
        } else if (state is SearchLoaded) {
          return ListView.builder(
            itemCount: state.catsFound.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                leading: SizedBox(
                  width: 60,
                  height: 60,
                  child: Stack(
                    children: <Widget>[
                      Center(
                        child: Container(
                          width: 60,
                          height: 60,
                          decoration: BoxDecoration(
                            color: const Color.fromARGB(228, 255, 255, 255),
                            // color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: const Icon(
                            Icons.image,
                            size: 50,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      Center(
                        child: ClipRRect(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(15)),
                          child: state.catsFound[index].image!.url != null
                              ? FadeInImage.memoryNetwork(
                                  placeholder: kTransparentImage,
                                  image: state.catsFound[index].image!.url!,
                                  fit: BoxFit.cover,
                                  height: 50,
                                  width: 50,
                                )
                              : const Icon(
                                  Icons.image,
                                  size: 50,
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
                title: Text(
                  state.catsFound[index].name!,
                ),
              );
            },
          );
        } else if (state is SearchLoading) {
          return const SizedBox(
            height: 350,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        } else if (state is SearchEmpty) {
          return const SizedBox(
            height: 350,
            child: Center(
              child: Text('No hay concidencias en la busqueda'),
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}
