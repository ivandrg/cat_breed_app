import 'package:flutter/material.dart';

import '../search/search_delegate.dart';



class SearchInputFieldWidget extends StatelessWidget {
  const SearchInputFieldWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final screensize = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () =>
          showSearch(context: context, delegate: VideoSearchDelegate()),
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).appBarTheme.backgroundColor,
          borderRadius: BorderRadius.circular(0),
          border: Border.all()
        ),
        // width: screensize.width * 0.9,
        // height: screensize.height * 0.06,
        height: 40,
        child: Row(
          children: [
            const Spacer(
              flex: 10,
            ),
            Text(
              'Buscar',
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            const Spacer(
              flex: 7,
            ),
            IconButton(
              onPressed: () =>
                  showSearch(context: context, delegate: VideoSearchDelegate()),
              icon: const Icon(Icons.search),
            ),
          ],
        ),
      ),
    );
  }
}
