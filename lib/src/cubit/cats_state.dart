part of 'cats_cubit.dart';

abstract class CatsState {}

class CatsInitial extends CatsState {}

class DataCatsLoading extends CatsState {}

class DataCatsLoaded extends CatsState {
  // ignore: prefer_typing_uninitialized_variables
  final data;

  DataCatsLoaded(this.data);
}

class DataCatsError extends CatsState {
  final String message;

  DataCatsError(this.message);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is DataCatsError && other.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}
