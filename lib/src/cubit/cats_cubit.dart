import 'package:bloc/bloc.dart';

import '../data/cat_data.dart';

part 'cats_state.dart';

class CatsCubit extends Cubit<CatsState> {
  CatsCubit() : super(DataCatsLoading());
  final catDataSource = DataCat();

  Future<void> getDataCats() async {
    try {
      final response = await catDataSource.getDataCats();
      emit(DataCatsLoaded(response));
    } on Failure catch (f) {
      emit(DataCatsError(f.message.toString()));
    }
  }
}
