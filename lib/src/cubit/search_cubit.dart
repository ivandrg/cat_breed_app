import 'package:bloc/bloc.dart';
import 'package:cats_app/src/data/cat_data.dart';
import 'package:cats_app/src/models/cat_data_model.dart';

part 'search_state.dart';

class SearchCubit extends Cubit<SearchState> {
  SearchCubit() : super(SearchInitial());
  final catDataSource = DataCat();

  Future<void> searchCat(String breed) async {
    try {
      emit(SearchLoading());
      List<CatDataModel> foundCats = [];
      List cats = await catDataSource.getDataCats();
      cats.add(cats[1]);
      for (var element in cats) {
        if (element.name.trim().toLowerCase() == breed.trim().toLowerCase()) {
          foundCats.add(element);
        }
      }
      if (foundCats.isNotEmpty) {
        emit(SearchLoaded(foundCats));
      } else {
        emit(SearchEmpty());
      }
    } on Failure catch (f) {
      emit(SearchError(f.message));
    }
  }

  Future<void> searchInitial() async {
    emit(SearchInitial());
  }
}
