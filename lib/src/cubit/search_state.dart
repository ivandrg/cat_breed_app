part of 'search_cubit.dart';

abstract class SearchState {}

class SearchInitial extends SearchState {}

class SearchEmpty extends SearchState {}

class SearchLoading extends SearchState {}

class SearchLoaded extends SearchState {
  final List<CatDataModel> catsFound;

  SearchLoaded(this.catsFound);
}

class SearchError extends SearchState {
  final String message;

  SearchError(this.message);

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SearchError && other.message == message;
  }

  @override
  int get hashCode => message.hashCode;
}
