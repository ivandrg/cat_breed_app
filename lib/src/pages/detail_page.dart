import 'package:cats_app/src/models/cat_data_model.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CatDataModel cat =
        ModalRoute.of(context)!.settings.arguments as CatDataModel;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back_ios_new,
            color: Colors.black,
          ),
        ),
        titleTextStyle: const TextStyle(
          color: Colors.black,
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
        centerTitle: true,
        title: Text(cat.name!),
      ),
      body: const DetailPageBody(),
    );
  }
}

class DetailPageBody extends StatefulWidget {
  const DetailPageBody({Key? key}) : super(key: key);

  @override
  State<DetailPageBody> createState() => _DetailPageBodyState();
}

class _DetailPageBodyState extends State<DetailPageBody> {
  ScrollController? _scrollController;
  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    CatDataModel cat =
        ModalRoute.of(context)!.settings.arguments as CatDataModel;
    const double side = 350;
    const double border = 0;
    const double padding = 15;
    final screensize = MediaQuery.of(context).size;
    return SizedBox(
      height: screensize.height,
      width: screensize.width,
      child: Column(
        children: [
          SizedBox(
            width: side,
            height: side - 50,
            child: Stack(
              children: <Widget>[
                Center(
                  child: Container(
                    width: side,
                    height: side,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(border),
                    ),
                    child: const Center(
                      child: Icon(
                        Icons.image,
                        size: 150,
                      ),
                    ),
                  ),
                ),
                Center(
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(border),
                    ),
                    child: cat.image!.url != null
                        ? FadeInImage.memoryNetwork(
                            placeholder: kTransparentImage,
                            // image: image!,
                            image: cat.image!.url!,
                            fit: BoxFit.cover,
                            width: side,
                            height: side,
                          )
                        : const Icon(
                            Icons.image,
                            size: 150,
                          ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.all(padding),
              child: Scrollbar(
                controller: _scrollController,
                thumbVisibility: true,
                child: ListView(
                  controller: _scrollController,
                  shrinkWrap: true,
                  children: [
                    Text(
                      cat.description!,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      cat.origin!,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      cat.intelligence.toString(),
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      cat.adaptability.toString(),
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      cat.lifeSpan!,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
