import 'package:cats_app/src/cubit/cats_cubit.dart';
import 'package:cats_app/src/models/cat_data_model.dart';
import 'package:cats_app/src/widgets/search_input_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:transparent_image/transparent_image.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return const Scaffold(
      body: LandingPageBody(),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class LandingPageBody extends StatefulWidget {
  const LandingPageBody({Key? key}) : super(key: key);

  @override
  State<LandingPageBody> createState() => _LandingPageBodyState();
}

class _LandingPageBodyState extends State<LandingPageBody> {
  @override
  void initState() {
    super.initState();
    context.read<CatsCubit>().getDataCats();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        const SizedBox(
          height: 15,
        ),
        Align(
          alignment: AlignmentDirectional.center,
          child: Text(
            'Catbreeds',
            style: Theme.of(context).textTheme.titleLarge,
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        const Padding(
          padding: EdgeInsets.all(5.0),
          child: SearchInputFieldWidget(),
        ),
        const SizedBox(
          height: 15,
        ),
        BlocBuilder<CatsCubit, CatsState>(
          builder: (context, state) {
            if (state is DataCatsLoading) {
              return const SizedBox(
                height: 350,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (state is DataCatsLoaded) {
              return ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: state.data.length,
                itemBuilder: (BuildContext context, int index) {
                  CatDataModel cat = state.data[index];
                  return CatCardWidget(
                    raza: cat.name,
                    inteligencia: cat.intelligence.toString(),
                    pais: cat.origin,
                    image: cat.image!.url,
                    cat: cat,
                  );
                },
              );
            }
            return const SizedBox();
          },
        ),
      ],
    );
  }
}

class CatCardWidget extends StatelessWidget {
  const CatCardWidget({
    Key? key,
    this.raza,
    this.pais,
    this.inteligencia,
    this.image,
    this.cat,
  }) : super(key: key);
  final String? raza;
  final String? pais;
  final String? inteligencia;
  final String? image;
  final CatDataModel? cat;

  @override
  Widget build(BuildContext context) {
    const double side = 300;
    const double border = 15;
    const double padding = 15;
    return Card(
      shape: const RoundedRectangleBorder(
        side: BorderSide(
          color: Colors.black,
          width: 1.0,
        ),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(padding),
            child: Row(
              children: [
                Text(
                  raza!,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                const Spacer(),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, 'detail', arguments: cat);
                  },
                  child: Text(
                    'Mas...',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            width: side,
            height: side,
            child: Stack(
              children: <Widget>[
                Center(
                  child: Container(
                    width: side,
                    height: side,
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(border),
                    ),
                    child: const Center(
                      child: Icon(
                        Icons.image,
                        size: 150,
                      ),
                    ),
                  ),
                ),
                Center(
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(border),
                    ),
                    child: image != null
                        ? FadeInImage.memoryNetwork(
                            placeholder: kTransparentImage,
                            // image: image!,
                            image: image!,
                            fit: BoxFit.cover,
                            width: side,
                            height: side,
                          )
                        : const Icon(
                            Icons.image,
                            size: 150,
                          ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(padding),
            child: Row(
              children: [
                Text(
                  pais!,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                const Spacer(),
                Text(
                  inteligencia!,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
