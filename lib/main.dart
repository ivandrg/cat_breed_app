import 'package:cats_app/src/cubit/cats_cubit.dart';
import 'package:cats_app/src/cubit/search_cubit.dart';
import 'package:cats_app/src/pages/landing_page.dart';
import 'package:cats_app/src/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CatsCubit>(
          create: (context) => CatsCubit(),
        ),
        BlocProvider<SearchCubit>(
          create: (context) => SearchCubit(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.black,
          iconTheme: const IconThemeData(
            color: Colors.black,
          ),
          // primarySwatch: Colors.blue,
          appBarTheme: const AppBarTheme(
            color: Colors.white,
            elevation: 0,
          ),
        ),
        home: const SafeArea(
          child: LandingPage(),
        ),
        routes: getApplicationRoutes(),
      ),
    );
  }
}
